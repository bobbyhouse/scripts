#!/bin/bash

FR_USERNAME='bobby@funraise.io'
FR_PASSWORD='Funraise1!'
FR_BASE_URL='http://localhost:9000'
FR_ENDPOINT="/api/v1/ref/gateway"
 
FR_AUTH_TOKEN=$(curl -s -X POST --data-urlencode  "username=$FR_USERNAME" --data-urlencode "password=$FR_PASSWORD" "$FR_BASE_URL/api/v1/login" -i | \
    grep "Set-Cookie" | \
    #authToken is the first value cookie separated by semicolons
    awk -F ';' '{print $1}' | \
    #remove the Set-Cookie prefix
    awk '{print $2}' | \
    #remove `authToken=`
    awk -F '=' '{print $2}'
)
  
req_body="
<gateway>
        <gateway_type>bitpay</gateway_type>
        <pairing_code>Bitpay Pairing Code</pairing_code>
        <client_name>Client Name</client_name>
        <bitpay_url>Bitpay Url</bitpay_url>
</gateway>"
 
echo "req_body: $req_body"
feature=$(curl -s \
    -X POST \
    -H "X-AUTH-TOKEN: $FR_AUTH_TOKEN" \
    -H "Content-Type: application/xml" \
    -d "$req_body" \
    "$FR_BASE_URL$FR_ENDPOINT")
