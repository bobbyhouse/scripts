#!/bin/bash

check_version()
{
    if [ "$1" != '"3.7.1"' ]
    then
        printf " ERROR"
        exit 1
    else
	printf "\n"
    fi
}


echo "checking platform"
for path in $(find / -path "*funraise/package.json" 2>/dev/null); do
 printf "$path"
 version=`echo $path | xargs grep "eslint-scope" | awk '{print $2}'`
 check_version $version
done

echo "checking asset build"
for path in $(find / -path "*funraise-asset-build/p2p/package.json" 2>/dev/null); do
 printf "$path"
 version=`echo $path | xargs grep "eslint-scope" | awk '{print $2}'`
 check_version $version
done

echo "checking funraise-ui"
for path in $(find / -path "*funraise-ui/package.json" 2>/dev/null); do
 printf "$path"
 version=`echo $path | xargs grep "eslint-scope" | awk '{print $2}'`
 check_version $version
done

echo "checking eslint-scope packages"
for path in $(find / -path "*/eslint-scope/package.json" 2>/dev/null); do
  printf "$path"
  version=`echo $path | xargs grep '"version":' | awk '{print $2}'`
  check_version $version
done

echo "You're safe to install the new NPM token"
